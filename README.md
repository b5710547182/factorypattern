Factory Pattern
================

by [Thanawit Gerdpraserd](https://bitbucket.org/b5710547182) and [Pipatpol Tanavongchinda](https://bitbucket.org/b5710546372)


##Pattern Name : Factory Pattern

##Context : Our program needs to create a lot of new Object
-----------------------------

Our program needs to create a new Object without the user know what Object will be create or how will we create that specific object, thus we can use this pattern to initialize the Object for us by creating the Factory method via interface or super class.


##Solution : 
---------------------

Create the Factory class consist of method that can create new Object that we desire by passing the parameter that can decide which object will be created. For that Method, use O-O style to make it easier to read and adjust the object with the similar behavior.

#Factory Pattern UML diagram
----------

![image_thumb.png](https://bitbucket.org/repo/xyLLAy/images/991794448-image_thumb.png)

From this UML, we can see that we create the Creator the Parent class and whenever any class wants to use the method it can extends and inherit from it , and then it can use the Product for interface to create new Object with similar behavior.

#Explain Method and Class in UML
---------

We use factoryMethod() in the Creator class to create new Object for and by using Product interface we can choose which Object with same behavior we will be create.

#Name in pattern and Actual name (Using Iteration as the Factory)
---------

Name in Factory Pattern| Actual Name
---------------------|-------------:
Factory | Collection
ConcreteFactory | A subclass of Collection
factoryMethod() | iterator()
Product | Iterator
ConcreteProduct | A subclass of iterator (which is often anonymous)

#Example of Factory Class
--------
![fac.png](https://bitbucket.org/repo/xyLLAy/images/1508486431-fac.png)

#Example in Real Life
------
Good example in my opinion is when you’re creating a game, because you want player to generate random enemy but in the same time the source code should be hide from player thus this pattern

is perfect for that solution. Just create [Example Code !](https://bitbucket.org/b5710546372/factory-method-pattern/src/b7a111f11c9b9a5ed1ce73cb719103f2d64ff479/src/?at=master)

##Amazon.java and AmazonProduct.java
----------

     This is the interface of all the Object that have the same 
     behavior will implements this class so that it easier to create 
     new Object by just calling this interface.

##Coffee.java and Greantea.java
-----------

After creating the Interface, we can use it to be implement by any class with same behavior that wish to be created by our Factory  

##KUgreenTea.java and CUcoffee.java
--------

This is the Factory class, in this case we have to Factory the first one is store named CUcoffee and the second one is KUgreenTea in order to make this class be able to.

##Main.java
------

And now we can create the Application class that call the Factory class to create the Object for itself.

#Example Exam (1)
-------

You are the game developer. Your customer want you to create FPS game that shoot UFO and Zombie. Create enemy factory that can make UFO and Zombie.

#Example Exam (2)
--------

You are Pâtisserie. You want to build a bakery factory that can make choux creme, croissant, eclair,and mont blanc.

[Here is a Example Exam Solution](https://bitbucket.org/b5710546372/factorymethodpattern-solution/src/f8aac80185049aa410147a9db44000b4f13a6153/src/?at=master)

#Consequence of using Factory Pattern
----------

Good :

* User doesn't know about all subclass in the program.
* Can randomly generate the class that user wants to create.
* Make code easier to read and use less line of code.
* Make the encapsulation easier.

Bad :

* Need some specific behavior to be useful.	
* If user doesn't know every subclass, it might cause some problem.
* Need Factory Class to function properly.